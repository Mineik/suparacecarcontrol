package tk.bradad.suparacecarinfo;

import android.annotation.SuppressLint;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import tk.bradad.suparacecarinfo.Util.BluetoothControlValidator;
import tk.bradad.suparacecarinfo.Util.BLEService;

public class MainActivity extends AppCompatActivity {

	@SuppressLint({"DefaultLocale", "SetTextI18n"})
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ProgressBar carBatteryStatus = (ProgressBar) findViewById(R.id.carBatteryStatus);
		carBatteryStatus.setMax(100);
		carBatteryStatus.setProgress(50);
		TextView battText = findViewById(R.id.battText);
		battText.setText(String.format("%s: %d", getString(R.string.batteryStatus),carBatteryStatus.getProgress())+"%");
		if(BluetoothControlValidator.canConnect((TextView) findViewById(R.id.BLEStatusText),this,this)){
			new BLEService((TextView) findViewById(R.id.InfoContainer), (Switch) findViewById(R.id.ScanSwitch), this);
		}
	}
}
