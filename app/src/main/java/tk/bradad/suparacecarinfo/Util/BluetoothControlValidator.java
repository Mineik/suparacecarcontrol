package tk.bradad.suparacecarinfo.Util;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.content.ContextCompat;
import tk.bradad.suparacecarinfo.R;

public class BluetoothControlValidator {
	public static boolean canConnect(TextView t, Activity a, Context c){
		if(hasPermit(a)){
			if(!hasFeature(c)){
				t.setText(R.string.cant);
				return false;
			}else {
				t.setText(R.string.can);
				return true;
			}
		}else{
			t.setText(R.string.permitnt);
			return false;
		}
	}

	public static boolean hasPermit(Activity a){
		if ((ContextCompat.checkSelfPermission(a, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED) ||(ContextCompat.checkSelfPermission(a, Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED)|(ContextCompat.checkSelfPermission(a, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
			return false;
		}
		else return true;
	}

	public static boolean hasFeature(Context context){
		if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
			return false;
		}
		else return true;
	}
}
