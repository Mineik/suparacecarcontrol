package tk.bradad.suparacecarinfo.Util;

import android.annotation.SuppressLint;
import android.bluetooth.*;
import android.content.Context;
import android.util.Log;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BLEService extends BluetoothGattCallback implements BluetoothAdapter.LeScanCallback{
	private BluetoothAdapter btAdapt;
	private TextView infoView;
	private List<BluetoothDevice> scannedDevices = new ArrayList<>();
	private String myDumbAssUUID = "08cb98fb-6798-4de7-8fff-35c56ad463e3";
	private BluetoothGatt Adolf;
	private Context context;

	public BLEService(TextView resultView, Switch scanSwitch, Context c){
		btAdapt = BluetoothAdapter.getDefaultAdapter();
		this.btAdapt.startLeScan(new UUID[]{UUID.fromString(this.myDumbAssUUID)},this);
		this.infoView = resultView;
		this.infoView.setText("Výsledek Skenování BT: \n");
		Log.d("Debug","Začínám sken");
		scannedDevices.clear();

		scanSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
			if(isChecked){
				btAdapt.startLeScan(BLEService.this);
			}else{
				try {
					btAdapt.stopLeScan(BLEService.this);
					Log.d("Debug", "Scanning stopped");
				} catch (NullPointerException exception) {
					Log.e("Debug", "Can't stop scan. Unexpected NullPointerException", exception);
				}
			}
		});
	}

	@SuppressLint("SetTextI18n")
	@Override
	public void onLeScan(BluetoothDevice device, int rssi, byte[]scanRecord){
		String MAC = device.getAddress();
		String BTName = device.getName();
		if(scannedDevices.size() != 0){
			if(scannedDevices.indexOf(device) == -1){ //if device is "unique"
				scannedDevices.add(device);
				this.infoView.setText(infoView.getText()+String.format("%s (%s)%n",BTName,MAC));
			}
		}else{
			scannedDevices.add(device);
			this.infoView.setText(infoView.getText()+String.format("%s (%s)%n",BTName,MAC));
		}
	}

	public boolean connect(String address) {
		Log.d("Debug", "Connecting to " + address);
		if (btAdapt == null || address == null) {
			Log.d("Debug", "BluetoothAdapter not initialized or unspecified address.");
			return false;
		}

		BluetoothDevice device = btAdapt.getRemoteDevice(address);

		// We want to directly connect to the device, so we are setting the autoConnect
		// parameter to false.
		BluetoothGatt bluetoothGatt = device.connectGatt(this.context, false, this);
		Log.d("Debug", "Trying to create a new connection.");
		this.Adolf = bluetoothGatt;
		return true;
	}

	@Override
	public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
		String address = gatt.getDevice().getAddress();

		if (newState == BluetoothProfile.STATE_CONNECTED) {
			Log.d("Debug", "Attempting to start service discovery:" + gatt.discoverServices());

		} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
//			onDisconnected(gatt);
			Log.d("Debug", "Disconnected from GATT server.");
		}
	}

	@Override
	public void onServicesDiscovered(BluetoothGatt gatt, int status) {
		if (status == BluetoothGatt.GATT_SUCCESS) {
			Log.d("Debug","Yay, we can talk to Adolf now");
			// success, we can communicate with the device
		} else {
			Log.d("Debug","Oopsies!");
			// failure
		}
	}

	public BluetoothGattCharacteristic findCharacteristic(UUID characteristicUUID) {
		BluetoothGatt bluetoothGatt = Adolf;

		if (bluetoothGatt == null) {
			return null;
		}

		for (BluetoothGattService service : bluetoothGatt.getServices()) {
			BluetoothGattCharacteristic characteristic = service.getCharacteristic(characteristicUUID);
			if (characteristic != null) {
				return characteristic;
			}
		}
		return null;
	}

	protected boolean readCharacteristic(BluetoothGattCharacteristic characteristic) {
		BluetoothGatt bluetoothGatt = Adolf;

		if (bluetoothGatt != null) {
			return bluetoothGatt.readCharacteristic(characteristic);

		}
		return false;
	}

	protected boolean writeCharacteristic(String address, BluetoothGattCharacteristic characteristic) { //NOTE TO SELF: ALWAYS SET VALUE OF CHARECTERISTIC USING BYTEARRAY (characteristic.setValue(byte[] value))
		BluetoothGatt bluetoothGatt = Adolf;

		if (bluetoothGatt != null) {
			return bluetoothGatt.writeCharacteristic(characteristic);

		}
		return false;
	}
}